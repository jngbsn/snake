#ifndef SNAKE_H
#define SNAKE_H

#include <vector>
#include "dot.h"

class Snake {

    public:
        int speed; //in ticks per move, 1 tick = 1 frame
        std::vector<Dot> dots;
        enum Dir {
            UP,
            RIGHT,
            DOWN,
            LEFT
        } dir;

        Snake(Dot firstDot);
        Snake(Dot firstDot, int speed);
        int draw(SDL_Surface* dst);
        void setDir(Dir d);
};

#endif

