#ifndef DOT_H
#define DOT_H
#include <SDL/SDL.h>

class Dot {

	protected:
		SDL_Surface* image;


	public:
	    SDL_Rect rect;
		Dot(SDL_Surface* img);
		void setXY(int x, int y);
		int draw(SDL_Surface* dst);

		bool operator==(const Dot& other);

};

#endif
