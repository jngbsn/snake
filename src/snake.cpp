#include "snake.h"


Snake::Snake(Dot firstDot) {
    dots.push_back(firstDot);
    speed = 60;
}

Snake::Snake(Dot firstDot, int spd) {
    dots.push_back(firstDot);
    speed = spd;
}

int Snake::draw(SDL_Surface* dst) {
    std::vector<Dot>::iterator itr;
    for(itr = dots.begin(); itr < dots.end(); ++itr) {
        itr->draw(dst);
    }
}

void Snake::setDir(Snake::Dir d) {
    dir = d;
}
