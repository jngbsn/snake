#define STATE_MENU 0
#define STATE_GAME 1
#define STATE_SCORES 2
#define STATE_DEAD 3

#include <cstdio>
#include <ctime>
#include <cstdlib>
#include "dot.h"
#include "snake.h"
#include "logo.h"

int blitSurface(SDL_Surface* src, SDL_Surface* dst, int x, int y) {
    SDL_Rect r = {x, y};
    SDL_BlitSurface(src, NULL, dst, &r);
}

SDL_Surface* drawLogo() {
    SDL_Surface* temp = SDL_CreateRGBSurface(SDL_SWSURFACE, 400, 80, 32, 0, 0, 0, 0);

    SDL_Surface* image = SDL_CreateRGBSurface(SDL_SWSURFACE, 16, 16, temp->format->BitsPerPixel, 0, 0, 0, 0);
    Uint32 color = SDL_MapRGB(image->format, 255, 255, 255);
    SDL_FillRect(image, NULL, color);

    Dot* dot = new Dot(image);
    dot->setXY(0, 0);

    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 25; ++j) {
            if(TEXT_logo[i][j] == 1) {
                dot->setXY(j*16, i*16);
                dot->draw(temp);
            }
        }
    }

    SDL_Flip(temp);
    SDL_FreeSurface(image);
    delete dot;
    return temp;
}

SDL_Surface* drawMenu() {
    SDL_Surface* temp = SDL_CreateRGBSurface(SDL_SWSURFACE, 112, 96, 32, 0, 0, 0, 0);

    SDL_Surface* image = SDL_CreateRGBSurface(SDL_SWSURFACE, 8, 8, temp->format->BitsPerPixel, 0, 0, 0, 0);
    Uint32 color = SDL_MapRGB(image->format, 255, 255, 255);
    SDL_FillRect(image, NULL, color);

    Dot dot = Dot(image);
    dot.setXY(0, 0);

    for(int i = 0; i < 12; ++i) {
        for(int j = 0; j < 14; ++j) {
            if(TEXT_menu[i][j] == 1) {
                dot.setXY(j*8, i*8);
                dot.draw(temp);
            }
        }
    }

    SDL_Flip(temp);
    SDL_FreeSurface(image);
    return temp;
}

SDL_Surface* drawMenuScreen() {

    SDL_Surface* temp = SDL_CreateRGBSurface(SDL_SWSURFACE, 640, 480, 32, 0, 0, 0, 0);

    SDL_Surface* logo = drawLogo();
    SDL_Rect a={((temp->w/2)-(logo->w/2)),100};
    SDL_BlitSurface(logo, NULL, temp, &a);

    SDL_Surface* menu = drawMenu();
    SDL_Rect b={((temp->w/2)-(menu->w/2)),300};
    SDL_BlitSurface(menu, NULL, temp, &b);

    SDL_Flip(temp);
    SDL_FreeSurface(logo);
    SDL_FreeSurface(menu);
    return temp;
}

SDL_Surface* draw321text() {

    SDL_Surface* temp = SDL_CreateRGBSurface(SDL_SWSURFACE, 144, 80, 32, 0, 0, 0, 0);

    SDL_Surface* image = SDL_CreateRGBSurface(SDL_SWSURFACE, 16, 16, temp->format->BitsPerPixel, 0, 0, 0, 0);
    SDL_FillRect(image, NULL, SDL_MapRGB(image->format, 255, 255, 255));

    Dot dot = Dot(image);
    dot.setXY(0, 0);

    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 9; ++j) {
            if(TEXT_321[i][j] == 1) {
                dot.setXY(j*16, i*16);
                dot.draw(temp);
            }
        }
    }

    SDL_Flip(temp);
    SDL_FreeSurface(image);
    return temp;

}

SDL_Surface* drawDead() {

    SDL_Surface* temp = SDL_CreateRGBSurface(SDL_SWSURFACE, 576, 160, 32, 0, 0, 0, 0);

    SDL_Surface* image = SDL_CreateRGBSurface(SDL_SWSURFACE, 32, 32, temp->format->BitsPerPixel, 0, 0, 0, 0);
    SDL_FillRect(image, NULL, SDL_MapRGB(image->format, 255, 255, 255));

    Dot dot = Dot(image);
    dot.setXY(0, 0);

    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 18; ++j) {
            if(TEXT_dead[i][j] == 1) {
                dot.setXY(j*32, i*32);
                dot.draw(temp);
            }
        }
    }

    SDL_Flip(temp);
    SDL_FreeSurface(image);
    return temp;

}

int main(int argc, char *argv[]) {
    srand(time(0));

	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_WM_SetCaption("Snake", "Snake");

	SDL_Surface* screen = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    SDL_Surface* dotImage = SDL_CreateRGBSurface(SDL_SWSURFACE, 16, 16, screen->format->BitsPerPixel, 0, 0, 0, 0);
    SDL_FillRect(dotImage, NULL, SDL_MapRGB(dotImage->format, 255, 255, 255));
    SDL_Surface* menuScreen = drawMenuScreen();
    SDL_Surface* threeTwoOne = draw321text();
    SDL_Surface* deadText = drawDead();

    bool done = false;
	SDL_Event event;
    int gamestate = STATE_MENU;
    int selection = 1;
    int score = 0;

    Dot dot = Dot(dotImage);

    Snake snake = Snake(Dot(dotImage), 10);
    snake.dots.push_back(Dot(dotImage));
    snake.dots.push_back(Dot(dotImage));
    snake.setDir(Snake::RIGHT);
    snake.dots[0].setXY(64,32);
    snake.dots[1].setXY(48,32);
    snake.dots[2].setXY(32,32);

    Dot food = Dot(dotImage);

    while(!done) {
        switch(gamestate) {
            case STATE_MENU: {
                if(SDL_PollEvent(&event)) {
                    switch(event.type) {
                        case SDL_KEYDOWN:
                            if(event.key.keysym.sym == SDLK_DOWN) {
                                if(selection == 1) selection = 2;
                            }
                            if(event.key.keysym.sym == SDLK_UP) {
                                if(selection == 2) selection = 1;
                            }
                            if(event.key.keysym.sym == SDLK_ESCAPE) {
                                done = true;
                            }
                            if(event.key.keysym.sym == SDLK_RETURN) {
                                if(selection == 1) gamestate = STATE_GAME;
                                if(selection == 2) done = true;
                            }
                            break;

                        case SDL_QUIT:
                            done = true;
                            break;

                        default:
                            break;
                    }
                }

                SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
                SDL_BlitSurface(menuScreen, NULL, screen, NULL);

                if(selection == 1) {
                    dot.setXY(264, 316);
                    dot.draw(screen);
                    dot.setXY(360, 316);
                    dot.draw(screen);
                } else if(selection == 2) {
                    dot.setXY(240, 372);
                    dot.draw(screen);
                    dot.setXY(384, 372);
                    dot.draw(screen);
                }

                SDL_Flip(screen);
                break;
            }

            case STATE_GAME: {

                food.setXY(((rand()%35)+5)*16, ((rand()%25)+5)*16);

                SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
                snake.draw(screen);
                food.draw(screen);
                SDL_Rect srcrect = {0,0,64,80};
                SDL_Rect dstrect = {(screen->w/2)-32, (screen->h/2)-40};
                SDL_BlitSurface(threeTwoOne, &srcrect, screen, &dstrect);
                SDL_Flip(screen);
                SDL_Delay(800);

                SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
                snake.draw(screen);
                food.draw(screen);
                srcrect = {64,0,64,80};
                SDL_BlitSurface(threeTwoOne, &srcrect, screen, &dstrect);
                SDL_Flip(screen);
                SDL_Delay(500);

                SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
                snake.draw(screen);
                food.draw(screen);
                srcrect = {128,0,16,80};
                dstrect = {(screen->w/2)-8, (screen->h/2)-40};
                SDL_BlitSurface(threeTwoOne, &srcrect, screen, &dstrect);
                SDL_Flip(screen);
                SDL_Delay(200);

                SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
                snake.draw(screen);
                food.draw(screen);
                srcrect = {288,300,64,40};
                dstrect = {(screen->w/2)-32, (screen->h/2)-20};
                SDL_BlitSurface(menuScreen, &srcrect, screen, &dstrect);
                SDL_Flip(screen);
                SDL_Delay(50);

                SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
                snake.draw(screen);
                food.draw(screen);
                SDL_Flip(screen);

                bool playingGame = true;
                int FPS = 60;
                int FPSinterval = 1000 / FPS;
                int nextFrame = SDL_GetTicks() + FPSinterval;
                int tick = 0;

                while(playingGame) {

                    if(tick == snake.speed) {

                        //logic
                        Dot* tempdot = new Dot(dotImage);
                        switch(snake.dir) {
                            case Snake::RIGHT: {
                                tempdot->setXY(snake.dots[0].rect.x + 16, snake.dots[0].rect.y);
                                break;
                            }
                            case Snake::LEFT: {
                                tempdot->setXY(snake.dots[0].rect.x - 16, snake.dots[0].rect.y);
                                break;
                            }
                            case Snake::UP: {
                                tempdot->setXY(snake.dots[0].rect.x, snake.dots[0].rect.y - 16);
                                break;
                            }
                            case Snake::DOWN: {
                                tempdot->setXY(snake.dots[0].rect.x, snake.dots[0].rect.y + 16);
                                break;
                            }
                        }
                        for(int i = (snake.dots.size()-1); i > 0; --i) {
                            snake.dots[i] = snake.dots[i-1];
                        }
                        snake.dots[0] = *tempdot;
                        delete tempdot;

                        if(snake.dots[0] == food) {
                            ++score;
                            snake.dots.push_back(snake.dots[snake.dots.size()-1]);
                            food.setXY(((rand()%35)+5)*16, ((rand()%25)+5)*16);
                            for(int i = snake.dots.size(); i > 0; --i) {
                                if(food == snake.dots[i]) {
                                    food.setXY(((rand()%35)+5)*16, ((rand()%25)+5)*16);
                                }
                            }
                            for(int i = snake.dots.size(); i > 0; --i) {
                                if(food == snake.dots[i]) {
                                    food.setXY(((rand()%35)+5)*16, ((rand()%25)+5)*16);
                                }
                            }
                            if((score%7) == 0) snake.speed -= 2;
                            if(snake.speed <= 1) snake.speed = 1; //you've been playing too much.
                        }
                        if(snake.dots[0].rect.x >= 640 || snake.dots[0].rect.x <= 0 || snake.dots[0].rect.y >= 480 || snake.dots[0].rect.y <= 0) {
                            gamestate = STATE_DEAD;
                            playingGame = false;
                        }
                        for(int i = snake.dots.size(); i > 0; --i) {
                            if(snake.dots[0] == snake.dots[i]) {
                                gamestate = STATE_DEAD;
                                playingGame = false;
                            }
                        }

                        //draw
                        SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
                        snake.draw(screen);
                        food.draw(screen);
                        SDL_Flip(screen);
                        tick = 0;
                    } else tick++;

                    if(SDL_PollEvent(&event)) {
                        switch(event.type) {
                            case SDL_KEYDOWN:
                                if(event.key.keysym.sym == SDLK_LEFT) {
                                    if(snake.dir != Snake::RIGHT) snake.dir = Snake::LEFT;
                                }
                                if(event.key.keysym.sym == SDLK_RIGHT) {
                                    if(snake.dir != Snake::LEFT) snake.dir = Snake::RIGHT;
                                }
                                if(event.key.keysym.sym == SDLK_DOWN) {
                                    if(snake.dir != Snake::UP) snake.dir = Snake::DOWN;
                                }
                                if(event.key.keysym.sym == SDLK_UP) {
                                    if(snake.dir != Snake::DOWN) snake.dir = Snake::UP;
                                }
                                if(event.key.keysym.sym == SDLK_ESCAPE) {
                                    playingGame = false;
                                    done = true;
                                }
                                break;

                            case SDL_QUIT:
                                playingGame = false;
                                done = true;
                                break;

                            default:
                                break;
                        }
                    }

                    if(nextFrame > SDL_GetTicks()) SDL_Delay((nextFrame - SDL_GetTicks()));
                    nextFrame = SDL_GetTicks() + FPSinterval;
                }
                break;
            }

            case STATE_SCORES: {

            }

            case STATE_DEAD: {

                SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
                blitSurface(deadText, screen, (screen->w/2)-(deadText->w/2), (screen->h/2)-(deadText->h/2));
                SDL_Flip(screen);
                SDL_Delay(1500);

                snake.dots.clear();
                snake.dots.push_back(Dot(dotImage));
                snake.dots.push_back(Dot(dotImage));
                snake.dots.push_back(Dot(dotImage));
                snake.setDir(Snake::RIGHT);
                snake.speed = 10;
                snake.dots[0].setXY(64,32);
                snake.dots[1].setXY(48,32);
                snake.dots[2].setXY(32,32);

                gamestate = STATE_MENU;
                break;
            }
        }
    }

    SDL_FreeSurface(menuScreen);
    SDL_FreeSurface(dotImage);
    SDL_FreeSurface(threeTwoOne);
	SDL_Quit();
	return 0;
}
