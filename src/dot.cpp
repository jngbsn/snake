#include "dot.h"

Dot::Dot(SDL_Surface* img) {
	image = img;
	rect.x = 0;
	rect.y = 0;
}

void Dot::setXY(int x, int y) {
	rect.x = x;
	rect.y = y;
}

int Dot::draw(SDL_Surface* dst) {
	SDL_BlitSurface(image, NULL, dst, &rect);
}

bool Dot::operator==(const Dot& other) {
    if((rect.x == other.rect.x) && (rect.y == other.rect.y)) return true;
    else return false;
}
